#ifndef __DADOS_TABELADOS_HPP__
#define __DADOS_TABELADOS_HPP__

#include <string>
#include <vector>

#include "LinhaDeCabecalho.hpp"
#include "LinhaDeDado.hpp"

class DadosTabelados {
    public:
        void LerArquivo(const std::string &file) ;

    private:
        LinhaDeCabecalho cabecalho;
        std::vector<LinhaDeDados> dados_;
};

#endif