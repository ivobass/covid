#include <iostream>
#include <sstream>
#include <string>

#include "LinhaDeCabecalho.hpp"

void LinhaDeCabecalho::Ler(std::istream &is) {
    constexpr char kComma{','};

    std::string line;
    std::string token;

    std::getline(is, line);

    std::stringstream ss{line};

    while(std::getline(ss, token, kComma)) {
        cabecalho_.push_back(token);
    }
}

void LinhaDeCabecalho::Escrever() {
    constexpr char kPipeDelimiter{'|'};

    char column_delimiter[2] = {'\0', '\0'};

    for(auto column : cabecalho_) {
        std::cout << column_delimiter << column;
        column_delimiter[0] = kPipeDelimiter;
    }

    std::cout << std::endl;
}
