#include <string>

#include "DadosTabelados.hpp"

int main(int argc, char **argv) {
    const std::string kDataSetCSV{"dados_diarios.csv"};

    DadosTabelados data;
    data.LerArquivo(kDataSetCSV);

    return EXIT_SUCCESS;
}
