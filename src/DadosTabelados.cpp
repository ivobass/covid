#include <iostream>

#include "DadosTabelados.hpp"

void DadosTabelados::LerArquivo(const std::string &file) {

    std::cout << "Abrindo arquivo '{" << file << "}'...\n";

    std::ifstream in;

    in.open(file);

    cabecalho.Ler(in);

    while(!in.eof()) {
        LinhaDeDados dado;
        dado.Ler(in);
        dados_.push_back(dado);
    }

    cabecalho.Escrever();

    for(auto d : dados_) {
        d.Escrever();
    }
}
