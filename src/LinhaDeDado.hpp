#ifndef __LINHA_DE_DADO__
#define __LINHA_DE_DADO__

#include <fstream>
#include <string>
#include <vector>

#include "Linha.hpp"

class LinhaDeDados : public Linha {
    public:
        void Ler(std::istream &is) override;
        void Escrever() override;

    private:
        std::vector<int> dados_;
};

#endif
