#include <iostream>
#include <sstream>
#include <string>

#include "LinhaDeDado.hpp"

void LinhaDeDados::Ler(std::istream &is) {
    auto remove_hyphen = [](const std::string &str)-> int {
        constexpr char kHyphenDelimiter{'-'};
        constexpr size_t kNumberCharacterRemoval{1};

        auto s = str;

        s.erase(s.find(kHyphenDelimiter), kNumberCharacterRemoval);
        s.erase(s.find(kHyphenDelimiter), kNumberCharacterRemoval);

        return std::stoi(s);
    };

    std::string line;
    std::string token;

    std::getline(is, line);

    std::stringstream ss{line};

    // Se a última linha está vazia, ela termina com '\n', não vamos prosseguir
    if (line.empty()) {
        return;
    }

    constexpr char kComma{','};

    std::getline(ss, token, kComma);

    // Data está no formato YYYY-MM-DD, temos que transformar em número inteiro
    dados_.push_back(remove_hyphen(token));

    // Restante das informações, já são números, só há necessidade de converter
    while(std::getline(ss, token, kComma)) {
        dados_.push_back(std::stoi(token));
    }
}

void LinhaDeDados::Escrever() {
    constexpr char kPipeDelimiter{'|'};

    char column_delimiter[2] = {'\0', '\0'};

    for(auto column : dados_) {
        std::cout << column_delimiter << column;
        column_delimiter[0] = kPipeDelimiter;
    }

    std::cout << std::endl;
}
