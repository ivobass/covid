#ifndef __LINHA_DE_CABECALHO__
#define __LINHA_DE_CABECALHO__

#include <fstream>
#include <string>
#include <vector>

#include "Linha.hpp"

class LinhaDeCabecalho : public Linha {
    public:
        void Ler(std::istream &is) override;
        void Escrever() override;

    private:
        std::vector<std::string> cabecalho_;
};

#endif