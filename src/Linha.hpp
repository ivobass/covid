#ifndef __LINHA_HPP__
#define __LINHA_HPP__

#include <fstream>

class Linha {
    public:
        virtual void Ler(std::istream &in) = 0;
        virtual void Escrever() = 0;
};

#endif